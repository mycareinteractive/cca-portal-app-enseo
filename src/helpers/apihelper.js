/**
 * Handy helper functions to get most common data from upserver
 * This class is a static class with all methods being static
 */
class ApiHelper {

    static getPreference(pref, scope) {
        scope = scope || 'PatientVisit';
        return upserver.api('/me/preferences/' + pref, 'GET', {
            scope: scope
        });
    }

    static setPreference(pref, val, scope) {
        scope = scope || 'PatientVisit';
        return upserver.api('/me/preferences', 'POST', {
            key: pref,
            value: val,
            scope: scope
        });
    }

}

export default ApiHelper;