import Pelican from 'pelican';
require('../css/common.css');
require('../css/edlibrary-synopsis.css');
var template = require('../templates/edsynopsisscreen.hbs');

const EDSynopsisScreen = Pelican.Screen.extend({

    className: 'edsynopsis',

    video: new Pelican.Models.Content(),

    template: template,

    keyEvents: {},

    events: {
        'click .back-button': 'back'
    },

    widgets: {

        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons',
            options: {orientation: 'vertical', preRender: true}
        }

    },

    calcDuration: function (duration) {
        if(!duration)
            return '00:00';
        var str = '';
        if(duration>=3600) {
            var h = Math.floor(duration/3600);
            str += h + ':';
            duration = duration%3600;
        }
        if(duration>=60) {
            var mm = Math.floor(duration/60);
            str += (mm<10?'0':'') + mm + ':';
            duration = duration%60;
        }
        var ss = duration;
        str += (ss<10?'0':'') + ss;
        return str;
    },

    getVideo: function(videoId){
        var self = this;
        var defer = jQuery.Deferred();

        var vid = videoId || self.vid;

        self.video.fetch({
            uri: '/contents',
            data:{
                id: vid
            }
        })
            .done(function(){
                self.state = 'loaded';
                console.log(self.video);
                defer.resolve();
            })
            .fail(function(f){
                defer.reject(f.errorMessage);
            });

        return defer.promise();
    },


    onInit: function () {
        var self = this;
        console.log('video queries: ' + JSON.stringify(self.queries));
        $('body').addClass('video');
        if (self.params.length > 1) {
            self.vid = self.params[0];
        }

        if (self.queries && self.queries['vid']) {
            self.vid = self.queries['vid'];
        }

        console.log('vid = ' + self.vid);

    },


    onScreenShow: function () {

        var self = this;
        self.lastPosition = 0;
        self.getVideo()
            .done(function () {

                if (self.video && self.video.attributes) {
                    self.bookmarkIds = self.video.attributes.bookmarkIds;
                    self.bookmarkId = self.queries['bookmarkId'] || self.video.attributes.bookmarkIds[0];
                    self.runTime = self.video.attributes.metadatas.runtime || 0;
                    self.title = self.video.attributes.metadatas.title;
                }


                var bookmark = new Pelican.Models.Bookmark({id: self.bookmarkId});
                self.lastPosition = 0;
                bookmark.fetch()
                    .done(function () {
                        console.log(bookmark);
                        self.lastPosition = bookmark.attributes.lastPosition;

                        self.duration = self.calcDuration(self.runTime);
                        self.$('#heading1 .text-large').html(self.title);
                        self.$('#heading2 .text-large').html(self.duration + ' mins');
                        self.$('#restart').attr('href', '#educationvideo/' + self.params[0] + '?bookmarkId=' + self.bookmarkId);
                        self.$('#resume').attr('href', '#educationvideo/' + self.params[0] + '?bookmarkId=' + self.bookmarkId + '&start=' + self.lastPosition* 1000 );
                    });
            });
    },

    onAttach: function () {

    }
});

export default EDSynopsisScreen;