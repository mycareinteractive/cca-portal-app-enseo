import Pelican from 'pelican';

require('../css/ehospital.css');

var template = require('../templates/ehospitalscreen.hbs');

const EHospitalScreen = Pelican.Screen.extend({

    className: 'ehospital',

    template: template,

    keyEvents: {
        'MENU': true,
        'EXIT': true,
        'BACK': true,
        'HOME': true,
        'CLOSE': true
    },

    onScreenShow: function() {
        var self = this;
        $.doTimeout(6000, function() {
            $('body').addClass('ehospital');
            self.$el.hide();
        });
    },

    onScreenHide: function() {
        var self = this;
        $('body').removeClass('ehospital');
    }

});

export default EHospitalScreen;