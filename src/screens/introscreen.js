import Pelican from 'pelican';
import ApiHelper from '../helpers/apihelper';

require('../css/global.css');
require('../css/common.css');
require('../css/intro.css');

var template = require('../templates/introscreen.hbs');

const IntroScreen = Pelican.Screen.extend({

    className: 'intro',

    template: template,

    keyEvents: {
        'POWER': true,
        'EXIT': true,
        'BACK': true,
        'HOME': true,
        'CLOSE': true
    },

    events: {
        'focus #english': 'highlightEnglish',
        'focus #spanish': 'highlightSpanish',
        'click #english': 'selectEnglish',
        'click #spanish': 'selectSpanish',
        'click #watchvideo': 'playWelcomeVideo'
    },

    widgets: {
        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons',
            options: { orientation: 'vertical', preRender: true }
        }
    },

    toggleMute: function() {

    },

    playWelcomeVideo: function() {
        this.log('play welcome video');
    },

    highlightWelcomeVideo: function() {
        this.log('welcome video highlighted');
    },

    highlightEnglish: function() {
        this.$('#instruction .text1').hide();
        this.$('#instruction .text1.english').show();
    },

    highlightSpanish: function() {
        this.$('#instruction .text1').hide();
        this.$('#instruction .text1.spanish').show();
    },

    selectEnglish: function(e) {
        e.preventDefault();
        console.log('select english');
        this.saveTerm();
        this.saveLanguage('en');
    },

    selectSpanish: function(e) {
        e.preventDefault();
        console.log('select english');
        this.saveTerm();
        this.saveLanguage('es');
    },

    saveTerm: function() {
        // term is visit level preference and should reset every time patient discharges
        ApiHelper.setPreference('term', 'accepted');
    },

    saveLanguage: function(lang) {
        if(!lang)
            return;

        // update language.
        App.data.language = lang;   // global option
        window.upserver.setLanguage(lang);  // set client API cookie
        window.upserver.locale = lang;  // set client API language header
        ApiHelper.setPreference('language', lang); // save to server

        // load new resource
        i18next.changeLanguage(lang, function() {
            App.layout.closeAllScreens();
            App.router.go('home');
        });

        // analytics
        App.tracker.event('language', 'set', lang);
    }
});

export default IntroScreen;