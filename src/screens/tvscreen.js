import Pelican from 'pelican';

require('../css/tv.css');
var channels = require('../data/channelmap.json');
var template = require('../templates/tvscreen.hbs');

const TVScreen = Pelican.Screen.extend({

    className: 'tv',
    template: template,

    channels: channels,
    channelIndex: -1,
    channelKeyed: null,
    currentChannel: {},
    dateBeg: null,
    dateEnd: null,
    dateNow: null,

    windowMode: true,
    windowModeX: 985,
    windowModeY: 25,
    windowModeWidth: 328,
    windowModeHeight: 185,
    fullscreenX: 0,
    fullscreenY: 0,
    fullscreenWidth: 1366,
    fullscreenHeight: 768,

    keyEvents: {},

    events: {},

    widgets: {},

    onInit: function (options) {
        var self = this;
        $('body').addClass('tv');
        // this.channels = channels;
    },

    playStartChannel: function() {
        // if channel number is passed in play the channel otherwise play the first one
        var ch = this.queries['channel'];
        if(ch) {
            this.playTvByNumber(ch);
        }
        else {
            this.playTv(0);
        }
    },

    stopTv: function() {
        console.log('Stopping channel...');
        $('body').removeClass('tv');
        this.$el.css('background', '#000000');
        PelicanDevice.stopChannel();
        // $('.screen.tvguide').show();    // HACK!! show the damn guide first
        // $.doTimeout('delay stop channel', 1000, function(){
        //     PelicanDevice.stopChannel();
        // });
    },

    playTv: function(idx) {
        if (!this.channels) {
            console.log('Channel list is empty, nothing to play.');
            return;
        }

        if (!this.channels[idx]) {
            console.log('Channel index ' + idx + ' out of bound.');
            return;
        }

        this.channelIndex = idx;
        this.currentChannel = this.channels[idx];
        App.data.lastChannel = this.currentChannel.number;
        var num = parseInt(this.currentChannel.number);

        console.log('We have officially changed the channel!');
        this.showLabel(num + ' - ' + this.currentChannel.name, 5000, true);

        console.log('Changing channel to ' + num + ' ...');
        if (App.device && App.device.getPlatform() != 'DESKTOP') {

            App.device.playChannel({
                "mode": "logicalnumber",
                "channelType": hcap.channel.ChannelType.RF,
                "logicalNumber": num,
                //"ipBroadcastType": hcap.channel.IpBroadcastType.UDP,
                "rfBroadcastType": hcap.channel.RfBroadcastType.CABLE,
                "onSuccess": function () {
                    console.log("channel change done.");
                },
                "onFailure": function (f) {
                    console.log("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }

        console.log('We are attempting to reset the selected channel: ' + this.currentChannel);
    },

    playTvByNumber: function(num) {
        console.log('Attempting to play ' + num);
        for (var i = 0; i < this.channels.length; i++) {
            if (this.channels[i].number == num) {
                this.currentChannel = this.channels[i];
                this.channelIndex = i;
                console.log('We are going to play the TV: ' + i);
                return this.playTv(i);
            }
        }
    },

    showLabel: function(text, ms, complete) {
        var self = this;
        ms = ms || 5000;
        this.$('#channel-label').removeClass('complete').show().find('#loadinginfo').text(text);
        if(complete) {
            this.$('#channel-label').addClass('complete');
        }
        $.doTimeout('tv label');
        $.doTimeout('tv label', ms, function () {
            self.$('#channel-label').hide();
            return false;
        });
    },

    keyChannel: function(key) {
        var self = this;
        var cLength = 1;
        $.doTimeout('keycheck');

        var pKey = self.channelKeyed;

        if (pKey) {
            self.channelKeyed = '';
            pKey = pKey + key;

            cLength = pKey.length;
            if (cLength == 3) {
                self.keyChannelDone(pKey);
                return pKey;
            }
            key = pKey;
        }

        if (cLength == 1 && key == '0')
            return;

        this.showLabel(key, 3000);

        self.channelKeyed = key;
        $.doTimeout('keycheck', 3000, function () {
            self.keyChannelDone(self.channelKeyed);
            return false;
        }.bind(this));
        return key;
    },

    keyChannelDone: function(key) {
        console.log('in keyChannelDone ' + key);
        $.doTimeout('keycheck');
        this.channelKeyed = '';
        this.playTvByNumber(key);
        return;
    },

    onScreenShow: function(){
        var self = this;

        console.log('onScreenShow: Showing the TV screen');
        $('body').addClass('tv');
        // a timer to keep tv page active on Analytics "Real-time" report.
        // Google real-time treats anything that has a hit in the past 5 minutes
        $.doTimeout('tv keep alive', 280000, function () {
            App.tracker.heartbeat();
            return true;
        });

        // play the channel
        if(!App.data.tvChannels){
            var url = "/lineups/" + App.config.lineups.tv + "/channels";
            console.log("Channel Lineup Request: " + url);

            App.upserver.api(url, 'GET')
                .done(function(data){
                    self.channels = data.sort(function(obj1, obj2) {
                        return obj1.number - obj2.number;
                    });

                    App.data.tvChannels = self.channels;

                    console.log('Channels Fetched');
                    console.log(self.channels);
                    self.playStartChannel();
                });
        }
        else {
            console.log('Using cached channels');
            self.channels = App.data.tvChannels;
            self.playStartChannel();
        }
    },

    onScreenHide: function(){
        console.log('onScreenHide: Hiding the TV screen');
        $.doTimeout('tv keep alive');
        $('body').removeClass('tv');
        this.stopTv();
    },

    onKey: function(e, key){
        var currentChannel = this.currentChannel;

        switch (key) {
            case 'ENTER':
                this.keyChannelDone(this.channelKeyed);
                break;
            case 'MENU':
            case 'BACK':
            case 'EXIT':
                $('body').removeClass('tv');
                this.back();
                // window.location.href = '#home';
                break;
            case 'UP':
            case 'CHUP':
                this.channelIndex++;
                if (this.channelIndex >= this.channels.length)
                    this.channelIndex = 0;
                this.playTv(this.channelIndex);
                break;
            case 'DOWN':
            case 'CHDN':
                this.channelIndex--;
                if (this.channelIndex < 0)
                    this.channelIndex = this.channels.length - 1;
                this.playTv(this.channelIndex);
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                this.keyChannel(key);
                break;
        }

        return true;
    },


    /* ******************************************
    *   #####################################
    *   ********  OLD FUNCTIONS  ************
    *   #####################################
    * ****************************************** */

    /*
    gotoChannel: function (chNum) {
        console.log('We are going to channel: ' + chNum);
        window.PelicanDevice.playChannel({
            type: "analog",
            rfType: "Cable",
            majorNumber: parseInt(chNum),
            minorNumber: 0
        });
        return false;
    },

    key2Channel: function (key) {
        console.log('in key2Channel ' + key)
        var keycheck = this.keycheck || undefined;
        // clearInterval(keycheck);
        this.keycheck = '';

        var pKey = this.key || undefined;
        this.key = '';

        this.key3Channel(pKey);
        return pKey;
    },

    key3Channel: function (key) {
        console.log('in key3Channel ' + key);
        this.gotoChannel(key);
        return;
    },

    keyChannel: function (key) {
        var cLength = 1;
        var keycheck = this.keycheck;

        if (keycheck) {
            clearTimeout(keycheck);
            this.keycheck = '';
        }

        var pKey = this.key;

        if (pKey) {
            this.key = '';
            pKey = pKey + key;

            cLength = pKey.length;
            if (cLength == 3) {
                this.key3Channel(pKey);
                return;
            }
            key = pKey;
        }

        if (cLength == 1 && key == '0')
            return;

        this.key = key
        var keycheck = setTimeout(function () {
            this.key2Channel(this.channelID)
        }.bind(this), 2000, this.channelID);

        this.keycheck = keycheck;

        return key;
    },

    onKey: function(e, key){

        var keyIndex = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'OK'].indexOf(key);

        if(!keyIndex)
            return false;

        if (keyIndex <= 9) {

            console.log('WE HAVE PRESSED THE KEY :: ' + key);

            var keyed = this.keyChannel(key);
            if ($("#channelStatus").is(':visible')) {
                $("#channel-label").show();
                $('#channelStatus').html(keyed);

            } else {
                $('#channelStatus').html(keyed);
                $('#channelStatus').show(50);
            }

            return true;
        }
    },

    onAttach: function () {
    }
    */
});

export default TVScreen;