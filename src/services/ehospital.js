/**
 * This class is specifically created for Cleveland EHospital LG TV solution
 */
class EHospital {

    start(options) {
        var self = this;

        this.input = 'TV';
        this.powerState = undefined;
        this.startParam = undefined;

        if (App.config.eHospital) {
            this.input = App.config.eHospital.externalInput;
        }

        window.upserver.on(window.upserver.events.device.hijackStart, this.hijackStart.bind(this));
        window.upserver.on(window.upserver.events.device.hijackStop, this.hijackStop.bind(this));
        window.upserver.on(window.upserver.events.device.hijackStatus, this.hijackStatus.bind(this));

        // when tv powers off restore to TV input
        document.addEventListener(
            "power_mode_changed",
            function () {
                console.log("Event 'power_mode_changed' is received");
                PelicanDevice.getPower()
                    .done(function (state) {
                        if (!state) { // powering off
                            // show home screen
                            App.layout.showBottomScreen();
                            // if input is not on 'TV', switch to 'TV' and send an hijack abort
                            PelicanDevice.getExternalInput()
                                .done(function (input) {
                                    if (input != 'TV' && input != 'Tuner') {
                                        if (self.startParam) {
                                            var resp = self.makeResponse(self.startParam);
                                            resp.statusCode = 'ABORT';
                                            resp.statusDescription = 'Hijack aborted by user';
                                            // send it in 5 seconds, giving TV a chance to finish work
                                            $.doTimeout(5000, function () {
                                                self.sendResponse(resp);
                                            });
                                        }
                                        PelicanDevice.setExternalInput('TV');
                                    }
                                });

                        }
                    });
            },
            false
        );
    }

    hijackStart(e, param) {
        var self = this;
        console.log('hijack start, param = ' + param);
        // close all screens first then go to hijack screen
        App.layout.showBottomScreen();
        App.router.go('ehospital');
        param = this.parseRequest(param);
        this.startParam = param;
        // give the other pages some time to finish like stopping video to avoid HDMI switching racing
        $.doTimeout(2000, function () {
            self.switchInput(param);
        });
    }

    hijackStop(e, param) {
        console.log('hijack stop, param = ' + param);
        this.startParam = undefined;
        App.layout.showBottomScreen();
        param = this.parseRequest(param);
        this.restoreInput(param);
    }

    hijackStatus(e, param) {
        console.log('hijack status, param = ' + param);
        param = this.parseRequest(param);
        this.checkInput(param);
    }

    switchInput(param) {
        var self = this;
        var resp = this.makeResponse(param);

        PelicanDevice.getPower()
            .done(function (state) {
                self.powerState = state;
                console.log('power state before hijacking: ' + state);
            })
            .fail(function (msg) {
                self.powerState = false;
                console.log('failed to get power state, assuming false.');
            })
            .always(function () {
                PelicanDevice.setPower(true).always(function () {
                    console.log('Switching to input: ' + self.input);
                    PelicanDevice.setExternalInput(self.input)
                        .done(function () {
                            resp.statusCode = 'OK';
                            resp.statusDescription = 'Hijack started';
                            self.sendResponse(resp);
                        })
                        .fail(function (msg) {
                            resp.statusCode = 'NOK';
                            resp.statusDescription = 'Fail to start hijack: ' + msg;
                            self.sendResponse(resp);
                        });
                });

            })
    }

    restoreInput(param) {
        var self = this;
        var resp = this.makeResponse(param);
        PelicanDevice.getExternalInput()
            .done(function (input) {
                if (input != 'TV' && input != 'Tuner') {
                    // If TV is on hijacking mode, restore to original state
                    PelicanDevice.setExternalInput('TV')
                        .done(function () {
                            resp.statusCode = 'OK';
                            resp.statusDescription = 'Hijack stopped';
                            self.sendResponse(resp);
                        })
                        .fail(function (msg) {
                            resp.statusCode = 'NOK';
                            resp.statusDescription = 'Fail to stop hijack ' + msg;
                            self.sendResponse(resp);
                        })
                        .always(function () {
                            if (self.powerState != undefined) {
                                console.log('restoring power state to ' + self.powerState);
                                PelicanDevice.setPower(self.powerState);
                                self.powerState = undefined;
                            }
                        });
                }
                else {
                    console.log('TV is already on "TV" input, ignore stop');
                    self.powerState = undefined;
                    resp.statusCode = 'OK';
                    resp.statusDescription = 'Hijack stopped';
                    self.sendResponse(resp);
                }
            })
            .fail(function (msg) {
                self.powerState = undefined;
                // If failed to get TV input, switch to TV anyway but don't touch power
                PelicanDevice.setExternalInput('TV')
                    .done(function () {
                        resp.statusCode = 'OK';
                        resp.statusDescription = 'Hijack stopped';
                        self.sendResponse(resp);
                    })
                    .fail(function (msg) {
                        resp.statusCode = 'NOK';
                        resp.statusDescription = 'Fail to stop hijack ' + msg;
                        self.sendResponse(resp);
                    });
            });

    }

    checkInput(param) {
        var self = this;
        var resp = this.makeResponse(param);
        PelicanDevice.getExternalInput()
            .done(function (input) {
                if (input == 'TV' || input == 'Tuner') {
                    // hijack ready
                    resp.statusCode = 'OK';
                    resp.statusDescription = 'Ready for Hijack';
                }
                else {
                    // on other external input
                    resp.statusCode = 'NOK';
                    resp.statusDescription = 'Hijack in progress';
                    self.sendResponse(resp);
                }
                self.sendResponse(resp);
            })
            .fail(function () {
                resp.statusCode = 'NOK';
                resp.statusDescription = 'Failed to check current TV input, please try later';
                self.sendResponse(resp);
            });
    }

    makeResponse(param) {
        var resp = {
            room: param.room,
            bed: param.bed,
            device: window.App.data.deviceId,
            action: param.action,
            callback: param.callback,
            statusCode: 'OK',
            statusDescription: ''
        };
        return resp;
    }

    parseRequest(param) {
        var obj = {};
        try {
            obj = JSON.parse(param);
        }
        catch (e) {
            console.log('Unable to parse hijack request: ' + e);
        }
        return obj;
    }

    sendResponse(resp) {
        console.log('Sending hijack response to server: ' + JSON.stringify(resp));

        $.post(App.config.server + '/partner/v1/ehospital/hijackcallback', resp)
            .done(function () {
                console.log('hijack response sent to server successfully');
            })
            .fail(function () {
                console.log("failed to send hijack response");
            });
    }

}

export default EHospital;