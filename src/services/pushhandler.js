class PushHandler {

    start(options) {
        upserver.on(upserver.events.device.patientAssociated, this.patientAssociated.bind(this));
        upserver.on(upserver.events.device.patientDissociated, this.patientDissociated.bind(this));
    }

    reset() {
        if (PelicanDevice.reboot) {
            console.log('About to reload...');
            PelicanDevice.reload();
        }
    }

    patientAssociated() {
        console.log('patient associated, will reload!');
        this.reset();
    }

    patientDissociated() {
        console.log('patient dessociated, will reload!');
        this.reset();
    }

}

export default PushHandler;