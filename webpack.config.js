var webpack = require('webpack');
var path = require('path');
var ES5to3OutputPlugin = require("es5to3-webpack-plugin");

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, '');
var config = require('./src/config.json');

var config = {
    debug: true,
    devtool: 'source-map',

    context: APP_DIR,

    entry: [
        './src/app.js'
    ],

    output: {
        filename: 'pelican-cca-avon-enseo.js',
        path: BUILD_DIR,
    },

    module: {
        loaders: [
            {test: /\.json$/, loader: 'json'},
            {
                test: /\.js(x)?$/, exclude: /node_modules/, loaders: ['babel'], plugins: [
                'transform-es3-property-literals',
                'transform-es3-member-expression-literals'
            ]
            },
            {test: /\.html$/, exclude: /node_modules/, loader: 'file?name=[name].[ext]'},
            {test: /\.(jpg|png|gif)$/, exclude: /node_modules/, loader: 'file?name=images/[name].[ext]'},
            {test: /\.css$/, loader: "style!css"},
            {test: /\.hbs$/, loader: "handlebars"},
            {test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'}
        ]
    },

    // resolve helps resolving 'pelican' as if it's a npm module
    resolve: {
        root: [
            path.resolve('./')
        ],
        alias: {
            // Always resolve backbone to the backbone package instead of package's own dependency.
            // https://github.com/webpack/webpack/issues/1165
            backbone: require.resolve('backbone'),
            jquery: "src/libs/jquery.js"
        }
    },

    // AMD tells webpack to expose jQuery: https://github.com/webpack/docs/wiki/FAQ
    amd: {
        jQuery: true
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: "src/libs/jquery.js",
            jQuery: "src/libs/jquery.js",
            Backbone: "backbone",
            Marionette: "backbone.marionette",
            Pelican: "pelican",
            i18next: "i18next"
        }),

        new ES5to3OutputPlugin()
    ],

    devServer: {
        // Note Opera 10.7 doesn't support HMR (--hot and --inline)
        disableHostCheck: true,
        host: "0.0.0.0",
        port: 3000,
        // Proxy helps reroute the request to your dev server
        proxy: [
            {
                context: ['/api/**', '/signalr/**'],
                target: config.webpackProxyTarget,
                secure: false
            }
        ]
    },

};

module.exports = config;